## Base

```sh
docker run --name nasa_keycloak --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak
```

```sh
docker build --build-arg REALM_FILE=nasa_realm.json -t nasa_keycloak_image .
docker run -e KEYCLOAK_SERVER_URL=http://localhost:8080/auth -p 8080:8080 nasa_keycloak_image
```

```dockerfile
FROM jboss/keycloak

ARG REALM_FILE

COPY $REALM_FILE /tmp/realm-config.json

USER root
RUN /opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin --password admin
RUN /opt/jboss/keycloak/bin/kcadm.sh create realms -f /tmp/realm-config.json
RUN rm /tmp/realm-config.json
```

```sh
curl -X POST "http://localhost:8080/auth/admin/realms" -H "Content-Type: application/json" -d @nasa_realm.json --user "admin:admin"
```

```sh
curl "http://localhost:8080/auth/admin/realms" --user "admin:admin"
curl -X POST "http://localhost:8080/auth/admin/realms" -H "Content-Type: application/json" -d @nasa_realm.json --user "admin:admin"
```

```sh
curl -k -H "Content-Type: application/x-www-form-urlencoded" -d "client_id=nasa-client" -d "username=bob" -d "password=1234" -d "grant_type=password" -X POST http://localhost:8080/realms/nasa-realm/protocol/openid-connect/token
```

## First try (I dont know how to async/wait in bash)

```shell
#!/bin/bash

KEYCLOAK_USER="admin"
KEYCLOAK_PASSWORD="admin"
REALM_JSON_FILE="nasa_realm.json"

docker run --rm -d --name nasa_keycloak -p 8080:8080 -e KEYCLOAK_USER=$KEYCLOAK_USER -e KEYCLOAK_PASSWORD=$KEYCLOAK_PASSWORD jboss/keycloak

# Wait Keycloak Container 
KEYCLOAK_CONTAINER_ID=$(docker ps -qf "name=nasa_keycloak")
KEYCLOAK_URL="http://localhost:8080/auth"

echo "Waiting for Keycloak to start..."
while true; do
    echo docker logs "$KEYCLOAK_CONTAINER_ID" | grep -q "started in"
    if docker logs "$KEYCLOAK_CONTAINER_ID" 2>&1 | grep -q "started in"; then
        break
    fi
    sleep 2
done

# Realm with curl
echo "Creating the realm..."
curl -X POST "$KEYCLOAK_URL/admin/realms" -H "Content-Type: application/json" -d "@$REALM_JSON_FILE" --user "$KEYCLOAK_USER:$KEYCLOAK_PASSWORD"

echo "Keycloak container logs:"
docker logs -f "$KEYCLOAK_CONTAINER_ID"
```

## Working


https://github.com/thomassuedbroecker/keycloak-create-realm-bash/tree/main/scripts


```shell
# docker 
./keycloak_run_docker.sh
# realm 
./keycloak_nasa_realm.sh  # once keycloak is up, working on ./keycloak_wait_container.sh
```
