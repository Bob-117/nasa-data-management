# Std

# Tk App
import tkinter as tk
from tkinter import ttk

# Custom
from .core import CustomLogger, NasaSession, get_token_with_creds, check_auth_dummy
from .frames import create_init_frame, create_auth_frame


class SubApp:
    def __init__(self, text_button, master):
        self.master = master
        self.text_button = text_button
        self.content = f'Content {text_button}'
        self.frame = None

    def create_frame(self):
        self.frame = tk.Frame(self.master)
        label = tk.Label(self.frame, text=self.content)
        label.pack()

    def show(self):
        self.frame.grid()

    def hide(self):
        self.frame.grid_remove()

    def __str__(self):
        return f'Sub app: {self.text_button}, content {self.content}'


def create_app_frame(master, sub_apps):
    def switch_sub_app(__sub_app):
        for app in sub_apps:
            app.hide()
        __sub_app.show()

    menu_frame = tk.Frame(master)
    menu_frame.grid(row=0, column=0)

    for idx, sub_app in enumerate(sub_apps):
        button = tk.Button(menu_frame, text=sub_app.text_button, command=lambda app=sub_app: switch_sub_app(app))
        button.grid(row=0, column=idx, padx=(60, 0))
        sub_app.create_frame()

    switch_sub_app(sub_apps[0])


class NasaAppWidget:
    def __init__(self):
        # Init
        self.log = CustomLogger('NasaAppWidget')
        # self.log_cli = CustomLoggerTerminal('NasaAppWidget')
        # self.log_file = CustomLoggerFile('NasaAppWidget_secret')
        self.log.info('Start App')
        # Authentication
        self.__session = NasaSession()
        # TK App Configuration
        self.width = 800
        self.height = 600
        self.__title = "NASA Data Management"
        self.root = tk.Tk()
        self.root.title(self.__title)
        self.root.geometry(f"{self.width}x{self.height}")
        # TK App Initialization
        self.init_frame, self.initial_button = None, None
        # TK App Authentication
        self.auth_frame, self.auth_button, self.auth_user_entries = None, None, None
        # TK App Main Frame
        self.app_frame = None
        # Create TK App
        self.set_frame()

    def start(self):
        self.root.mainloop()

    def set_frame(self):
        # Start
        self.init_frame, self.initial_button = create_init_frame(
            _root=self.root, _start_method=self.start_app, _w=self.width, _h=self.height
        )
        # Auth
        self.auth_frame, self.auth_button, self.auth_user_entries = create_auth_frame(
            _root=self.root, _auth_method=self.__auth, _w=self.width, _h=self.height
        )
        self.auth_frame.grid_remove()
        # Main App
        self.__set_app_frame()

    # def __set_app_frame(self):
    #     # Instead of this :
    #     self.app_frame = ttk.Frame(self.root)
    #     GIBSON = tk.Label(self.app_frame, text="GIBSON")
    #     GIBSON.grid()

    def __set_app_frame(self):
        self.app_frame = ttk.Frame(self.root)
        GIBSON = tk.Label(self.app_frame, text="GIBSON")
        GIBSON.grid()

        sub_apps = [
            SubApp(text_button='Databases', master=self.app_frame),
            SubApp(text_button='Gather', master=self.app_frame),
            SubApp(text_button='Pandas', master=self.app_frame)
        ]

        create_app_frame(self.app_frame, sub_apps)

    def start_app(self):
        self.init_frame.grid_remove()
        self.auth_frame.grid()

    def __auth(self):
        user_name_value = self.auth_user_entries.get('form_entry_user').get()
        user_pass_value = self.auth_user_entries.get('form_entry_pass').get()
        try:
            self.__session.token = get_token_with_creds(user_name_value, user_pass_value)

        except Exception as e:  # todo urllib3.connection.HTTPConnection
            self.log.error(f'Cant find Token : {e}')
            try:
                self.log.info('trying dummy')
                check_auth_dummy(user_name_value, user_pass_value)
            except Exception as e:
                self.log.error(e.message)
                self.log.info('Cant Login')
                return  # todo
        self.log.info('Login success')
        self.auth_frame.grid_remove()
        self.app_frame.grid()


if __name__ == '__main__':
    NasaAppWidget().start()
