from tkinter import ttk


def create_init_frame(_root, _start_method, _w, _h):
    initial_frame = ttk.Frame(master=_root)
    initial_frame.grid(row=0, column=0, padx=10, pady=10)
    initial_button = ttk.Button(master=initial_frame, text="Start App", command=_start_method, width=10)
    initial_button.grid(row=0, column=0, padx=(_w / 2) - 50, pady=(_h / 2) - 50)  # 10*width/2
    initial_button.grid(columnspan=3, rowspan=3)
    return initial_frame, initial_button
