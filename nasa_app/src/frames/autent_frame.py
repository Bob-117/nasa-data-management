import tkinter as tk

from tkinter import ttk


def create_auth_frame(_root, _auth_method, _w, _h):
    auth_frame = ttk.Frame(master=_root, width=10)

    form_label_user = tk.Label(auth_frame, text="User :")
    form_entry_user = tk.Entry(auth_frame, width=10)
    form_label_user.grid(row=0, column=0, padx=(0, 80), sticky='n')
    form_entry_user.grid(row=0, column=1)

    form_label_pass = tk.Label(auth_frame, text="Pass :")
    form_entry_pass = tk.Entry(auth_frame, width=10, show="*")
    form_label_pass.grid(row=1, column=0, padx=(0, 80), sticky='n')
    form_entry_pass.grid(row=1, column=1)

    auth_button = tk.Button(auth_frame, text="Auth", command=_auth_method, width=10)
    auth_button.grid(row=2, column=0, columnspan=2, pady=10)

    auth_frame.grid(row=2, column=2, padx=((_w / 2) - 100), pady=((_h / 2) - 50))

    form_entry_user = form_entry_user
    form_entry_pass = form_entry_pass
    return auth_frame, auth_button, {'form_entry_user': form_entry_user, 'form_entry_pass': form_entry_pass}
