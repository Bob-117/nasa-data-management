import tkinter as tk
from tkinter import ttk


def create_subframe(parent_frame, frame_name):
    subframe = ttk.Frame(parent_frame)

    label = tk.Label(subframe, text=f"Content for Frame {frame_name}:")
    label.grid(row=0, column=0, padx=10, pady=10)

    entry = tk.Entry(subframe, width=20)
    entry.grid(row=1, column=0, padx=10, pady=10)

    return subframe


def create_main_frame(_root, w, h):
    main_frame = ttk.Frame(_root)

    frame_a = create_subframe(main_frame, 'A')
    frame_b = create_subframe(main_frame, 'B')
    frame_c = create_subframe(main_frame, 'C')

    def show_frame_a():
        frame_a.grid(row=2, column=0, padx=10, pady=10)
        frame_b.grid_forget()
        frame_c.grid_forget()

    def show_frame_b():
        frame_b.grid(row=2, column=0, padx=10, pady=10)
        frame_a.grid_forget()
        frame_c.grid_forget()

    def show_frame_c():
        frame_c.grid(row=2, column=0, padx=10, pady=10)
        frame_a.grid_forget()
        frame_b.grid_forget()

    button_a = ttk.Button(main_frame, text="Show Frame A", command=show_frame_a)
    button_b = ttk.Button(main_frame, text="Show Frame B", command=show_frame_b)
    button_c = ttk.Button(main_frame, text="Show Frame C", command=show_frame_c)

    button_a.grid(row=0, column=0, padx=(0, 0), pady=10)
    button_b.grid(row=0, column=1, padx=(0, 0), pady=10)
    button_c.grid(row=0, column=2, padx=(0, 0), pady=10)

    show_frame_a()

    return main_frame


def process():
    root = tk.Tk()
    root.geometry("600x400")
    root.title("Frame Switching Example")

    main_frame = create_main_frame(root, 600, 400)
    main_frame.grid(row=1, column=0)

    root.mainloop()


if __name__ == '__main__':
    process()
