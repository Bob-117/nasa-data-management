import tkinter as tk
from tkinter import PhotoImage


def create_colored_frame(root, color, image_path):
    frame = tk.Frame(root, bg=color)
    frame.pack(fill='both', expand=True)

    img = PhotoImage(file=image_path)
    image_label = tk.Label(frame, image=img)
    image_label.image = img
    image_label.pack()

    return frame


def main():
    root = tk.Tk()
    root.title("Colored Frame with Image")

    background_color = 'lightblue'

    image_path = '/home/bob/Images/gitlab_halo.jpg'

    create_colored_frame(root, background_color, image_path)

    root.geometry("400x300")
    root.mainloop()


if __name__ == "__main__":
    # todo _tkinter.TclError: couldn't recognize data in image file "/home/bob/Images/gitlab_halo.jpg"
    main()
