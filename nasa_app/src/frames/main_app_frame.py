import tkinter as tk
from tkinter import ttk


class MainApp:
    def __init__(self, master):
        self.root = tk.Frame(master)
        self.menu = None
        self.frame = None


class SubApp:
    def __init__(self, text_button, master):
        self.master = master
        self.text_button = text_button
        self.content_text = f'Content {text_button}'
        self.frame = None
        self.content = None

    # def create_frame(self):
    #     self.frame = tk.Frame(self.master)
    #     # ## Content
    #     self.content = tk.Label(self.frame, text=self.content_text)
    #     # self.content = tk.Frame(self.frame)
    #     self.content.grid(row=2, column=0)
    #     self.content.grid_remove()
    #     # ##
    #     self.frame.grid(row=1, column=0)
    #     self.hide()

    def create_frame(self):
        self.frame = tk.Frame(self.master)
        self.frame.grid(row=1, column=0, sticky='nsew')

        # Button
        button = tk.Button(self.frame, text=self.text_button, command=self.show)
        button.grid(row=0, column=0, sticky='w')
        self.frame.grid_remove()
        # Content
        self.content = tk.Label(self.frame, text=self.content_text)
        self.content.grid(row=1, column=0, sticky='nsew')
        self.hide()

    def show(self):
        self.frame.grid()
        self.content.grid()

    def hide(self):
        self.frame.grid_remove()
        self.content.grid_remove()

    def __str__(self):
        return f'Sub app: {self.text_button}, content {self.content_text}'


def create_app_frame(_root, _sub_apps):
    _app_frame = ttk.Frame(master=_root)

    def switch_sub_app(__sub_app):
        for __app in _sub_apps:
            __app.hide()
        __sub_app.show()

    menu_frame = tk.Frame(_app_frame)
    menu_frame.grid(row=0, column=0)

    for idx, sub_app in enumerate(_sub_apps):
        button = tk.Button(menu_frame, text=sub_app.text_button, command=lambda app=sub_app: switch_sub_app(app))
        button.grid(row=0, column=idx)
        sub_app.create_frame()

    switch_sub_app(_sub_apps[0])
    return _app_frame


def process():
    app_frame = tk.Tk()
    app_frame.title("Application Frame")
    app_frame.geometry('500x500')

    sub_apps = [
        SubApp(text_button='databases', master=app_frame),
        SubApp(text_button='gather', master=app_frame),
        SubApp(text_button='pandas', master=app_frame)
    ]

    create_app_frame(app_frame, sub_apps)
    app_frame.mainloop()


if __name__ == '__main__':
    process()
