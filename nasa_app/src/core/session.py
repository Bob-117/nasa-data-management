import datetime
from .logger import CustomLogger


class NasaSession:
    _user: str
    _pass: str
    _token: str
    _historic: dict

    def __init__(self):
        self.nasa_session_log = CustomLogger('NasaSession')
        self._start_time = datetime.datetime.now()
        self._token = ''

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, value):
        self._user = value

    @property
    def password(self):
        return self._pass

    @password.setter
    def password(self, value):
        self._pass = value

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, value):
        # self._historic[datetime.datetime.now()] = f'Change token from {self.token} to {value}'
        self._token = value

    def __str__(self):
        return f'TOKEN : {self.token}'

    # @property
    # def _authentication_time(self):
    #     return self._token
    #
    # @_authentication_time.setter
    # def _authentication_time(self, value):
    #     self.nasa_session_log.info(f'Authentication : {self.user} at {value}')
    #     self._token = value

    def kill(self):
        ...
