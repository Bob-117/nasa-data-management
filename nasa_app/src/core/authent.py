import hashlib

import keycloak
from keycloak import KeycloakOpenID

from .logger import CustomLogger


class AuthErr(Exception):
    def __init__(self, _msg='Auth Error'):
        self.message = _msg


auth_logger = CustomLogger('auth_logger')


keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="nasa-client",  # from realm.json
    realm_name="nasa-realm",  # from realm.json
    client_secret_key="nasa_secret"  # from realm.json, but don't do that
)


def get_token_with_creds(username: str, password: str):
    try:
        token = keycloak_openid.token(username, password, grant_type="password")
        auth_logger.info(f'Token received: {token}')
        return token
    except keycloak.exceptions.KeycloakAuthenticationError:
        auth_logger.error('KeycloakAuthenticationError')
        raise keycloak.exceptions.KeycloakAuthenticationError(error_message="Invalid credentials")


def check_auth_dummy(username: str, password: str):
    users = {
        "admin": "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
    }
    sha256 = hashlib.sha256()
    sha256.update(str(password).encode('utf-8'))
    if users.get(username) is None or users.get(username) != sha256.hexdigest():
        auth_logger.error('Dummy Auth Failed')
        raise AuthErr(_msg='Dummy Auth Error')
