from dependency_injector.wiring import inject, Provide
from fastapi import APIRouter, Depends
from starlette import status
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import Response

from containers import Container
from services import FireBallService
from models import FireBall
from repositories import NotFoundError

router = APIRouter()


@router.get("/ping")
def root():
    return {"PONG !"}


@router.get("/fireballs")
@inject
def get_list(
        fireball_service: FireBallService = Depends(Provide[Container.fireball_service]),
):
    return fireball_service.get_fireballs()


@router.get("/fireballs/{fireball_id}")
@inject
def get_by_id(
        fireball_id: int,
        fireball_service: FireBallService = Depends(Provide[Container.fireball_service]),
):
    try:
        return fireball_service.get_fireball_by_id(fireball_id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.post("/fireballs", status_code=status.HTTP_201_CREATED)
@inject
async def add(request: Request, fireball_service: FireBallService = Depends(Provide[Container.fireball_service])):
    fireball_data = await request.json()
    required_fields = ['energy', 'impact_e', 'lat', 'lat_dir', 'lon', 'alt', 'vel']
    for field in required_fields:
        if field not in fireball_data:
            raise HTTPException(status_code=400, detail=f"Le champ {field} est requis")
    fireball = FireBall(**fireball_data)
    return fireball_service.create_fireball(fireball)


@router.patch("/fireballs/{fireball_id}", status_code=status.HTTP_200_OK)
@inject
async def update(
        fireball_id: int,
        request: Request,
        fireball_service: FireBallService = Depends(Provide[Container.fireball_service]),
):
    fireball_data = await request.json()
    try:
        return fireball_service.update_fireball(fireball_id, fireball_data)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.delete("/fireballs/{fireball_id}", status_code=status.HTTP_204_NO_CONTENT)
@inject
def remove(
        fireball_id: int,
        fireball_service: FireBallService = Depends(Provide[Container.fireball_service]),
):
    try:
        fireball_service.delete_user_by_id(fireball_id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
