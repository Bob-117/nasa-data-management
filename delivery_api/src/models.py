from sqlalchemy import Column, String, Boolean, Integer, Double

from database import Base


class FireBall(Base):
    __tablename__ = "fire_ball"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    date = Column(String)
    energy = Column(Double)
    impact_e = Column(Double)
    lat = Column(Double)
    lat_dir = Column(String(1))
    lon = Column(Double)
    lon_dir = Column(String(1))
    alt = Column(Double)
    vel = Column(Double)

    def __repr__(self):
        return f"<FireBall(id={self.id}, " \
               f"date=\"{self.date}\", " \
               f"energy=\"{self.energy}\", " \
               f"impact_e=\"{self.impact_e}\", " \
               f"lat=\"{self.lat}\", " \
               f"lat_dir=\"{self.lat}\", " \
               f"lon=\"{self.lat}\", " \
               f"lon_dir=\"{self.lat}\", " \
               f"alt=\"{self.lat}\", " \
               f"vel={self.lat_dir})>"
