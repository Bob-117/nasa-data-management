from contextlib import AbstractContextManager
from typing import Callable, Iterator
from api_logger import CustomLogger
from sqlalchemy.orm import Session
from models import FireBall

api_logger = CustomLogger('save_logger')


class FireBallRepository:

    def __init__(self, session_factory: Callable[..., AbstractContextManager[Session]]) -> None:
        self.session_factory = session_factory

    def get_all(self) -> Iterator[FireBall]:
        try:
            with self.session_factory() as session:
                return session.query(FireBall).all()
        except Exception as e:
            api_logger.error(e)

    def get_by_id(self, fireball_id: int) -> FireBall:
        try:
            with self.session_factory() as session:
                fireball = session.query(FireBall).filter(FireBall.id == fireball_id).first()
                if not fireball:
                    raise FireBallNotFoundError(fireball_id)
                return fireball
        except Exception as e:
            api_logger.error(e)

    def add(self, fireball) -> FireBall:
        with self.session_factory() as session:
            fireball = FireBall(date=fireball.date, energy=fireball.energy, impact_e=fireball.impact_e,
                                lat=fireball.lat, lat_dir=fireball.lat_dir, lon=fireball.lon, lon_dir=fireball.lon_dir,
                                alt=fireball.alt, vel=fireball.vel)
        try:
            session.add(fireball)
            session.commit()
            session.refresh(fireball)
            api_logger.info(f"Fireball {fireball.id} created")
            return fireball
        except Exception as e:
            api_logger.error(e)

    def update(self, fireball_id: int, fireball_data: dict) -> FireBall:
        with self.session_factory() as session:
            fireball = session.query(FireBall).filter(FireBall.id == fireball_id).first()
            if not fireball:
                raise FireBallNotFoundError(fireball_id)
            if not fireball_data:
                return fireball
            for key, value in fireball_data.items():
                if hasattr(fireball, key):
                    setattr(fireball, key, value)
                else:
                    raise InvalidFieldError(f"Champ non valide : {key}")
        try:
            session.commit()
            session.refresh(fireball)
            api_logger.info(f"Fireball {fireball.id} updated")
            return fireball
        except Exception as e:
            api_logger.error(e)

    def delete_by_id(self, fireball_id: int) -> None:
        try:
            with self.session_factory() as session:
                entity: FireBall = session.query(FireBall).filter(FireBall.id == fireball_id).first()
                if not entity:
                    raise FireBallNotFoundError(fireball_id)
                session.delete(entity)
                session.commit()
                api_logger.info(f"Fireball {fireball_id} deleted")
                return
        except Exception as e:
            api_logger.error(e)


class NotFoundError(Exception):
    entity_name: str

    def __init__(self, entity_id):
        super().__init__(f"{self.entity_name} not found, id: {entity_id}")


class InvalidFieldError:
    field_name: str

    def __init__(self, entity_id):
        super().__init__(f"{self.field_name} not found, id: {entity_id}")


class FireBallNotFoundError(NotFoundError):
    entity_name: str = "FireBall"
