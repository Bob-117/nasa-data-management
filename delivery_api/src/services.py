from typing import Iterator
from repositories import FireBallRepository
from models import FireBall

from api_logger import CustomLogger

api_logger = CustomLogger('save_logger')


class FireBallService:

    def __init__(self, fireball_repository: FireBallRepository) -> None:
        self._repository: FireBallRepository = fireball_repository

    def get_fireballs(self) -> Iterator[FireBall]:
        try:
            query = self._repository.get_all()
            return query
        except Exception as e:
            api_logger.error(e)

    def get_fireball_by_id(self, fireball_id: int) -> FireBall:
        try:
            if not fireball_id:
                raise ValueError('fireball_id is required')
            query = self._repository.get_by_id(fireball_id)
            return query
        except Exception as e:
            api_logger.error(e)

    def create_fireball(self, fireball: FireBall) -> FireBall:
        if not fireball:
            raise ValueError('fireball is required')
        try:
            query = self._repository.add(fireball)
            return query
        except Exception as e:
            api_logger.error(e)

    def update_fireball(self, fireball_id: int, fireball_data: dict) -> FireBall:
        if not fireball_id:
            raise ValueError('fireball_id is required')
        if not fireball_data:
            raise ValueError('fireball_data is required')
        try:
            query = self._repository.update(fireball_id, fireball_data)
            return query
        except Exception as e:
            api_logger.error(e)

    def delete_fireball_by_id(self, fireball_id: int) -> None:
        if not fireball_id:
            raise ValueError('fireball_id is required')
        try:
            query = self._repository.delete_by_id(fireball_id)
            return query
        except Exception as e:
            api_logger.error(e)

