import os
import logging

from dependency_injector import containers, providers
from database import Database
from repositories import FireBallRepository
from services import FireBallService

logging.basicConfig(level=logging.INFO)


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(modules=["endpoints"])

    # Load the database URL from the DATABASE_URL environment variable
    db_url = os.getenv("DATABASE_URL")
    logging.info(f'db url env : {db_url}')
    if db_url:
        db = providers.Singleton(Database, db_url=db_url)
    else:
        config = providers.Configuration(yaml_files=["config.yml"])
        db = providers.Singleton(Database, db_url=config.db.url)

    fireball_repository = providers.Factory(
        FireBallRepository,
        session_factory=db.provided.session,
    )

    fireball_service = providers.Factory(
        FireBallService,
        fireball_repository=fireball_repository,
    )
