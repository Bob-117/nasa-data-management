from contextlib import contextmanager, AbstractContextManager
from typing import Callable
from sqlalchemy import create_engine, orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from api_logger import CustomLogger

Base = declarative_base()

api_logger = CustomLogger('api_logger')


class Database:

    def __init__(self, db_url: str) -> None:
        self._engine = create_engine(db_url, echo=True)
        self._session_factory = orm.scoped_session(
            orm.sessionmaker(
                autocommit=False,
                autoflush=False,
                bind=self._engine,
            ),
        )

    def create_database(self) -> None:
        Base.metadata.create_all(self._engine)
        api_logger.info(f'Creating database at {self._engine.url}')

    @contextmanager
    def session(self) -> Callable[..., AbstractContextManager[Session]]:
        session: Session = self._session_factory()
        try:
            yield session
            api_logger.info(f'Session created : {session}')
        except Exception:
            session.rollback()
            api_logger.error(f'Error during session : {session}')
            raise
        finally:
            session.close()
            api_logger.info(f'Session closed : {session}')
