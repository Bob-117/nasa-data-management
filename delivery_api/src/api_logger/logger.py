import logging

api_logger = logging.getLogger("api_logger")
api_logger.setLevel(logging.INFO)


class CustomLogger:
    def __init__(self, name):
        self.logger = logging.getLogger(name)
        logging.basicConfig(
            level=logging.INFO
        )

    def info(self, message):
        self.logger.info(message)

    def error(self, message):
        self.logger.error(message)

    def warning(self, message):
        self.logger.warning(message)
