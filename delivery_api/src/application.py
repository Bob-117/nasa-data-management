import os

import uvicorn
from fastapi import FastAPI
from containers import Container
from endpoints import router
from api_logger import CustomLogger

api_logger = CustomLogger("api_logger")


def create_app() -> FastAPI:
    container = Container()
    app = FastAPI()
    app.container = container
    app.include_router(router)
    return app


app = create_app()

if __name__ == '__main__':
    host = os.getenv("UVICORN_HOST", "0.0.0.0")
    port = int(os.getenv("UVICORN_PORT", "8001"))
    api_logger.info(f"App running on http:/localhost:{port}/")
    api_logger.info(f"Go to http:/localhost:{port}/ping to test.")
    uvicorn.run(app, host=host, port=port)

