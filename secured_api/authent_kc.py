from fastapi import HTTPException
from keycloak import KeycloakOpenID, KeycloakAuthenticationError

keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="nasa-client",  # from json
    realm_name="nasa-realm",  # from json
    client_secret_key="nasa_secret"  # from json, but don't do that
)


def get_token_with_creds(username: str, password: str):
    try:
        token = keycloak_openid.token(username, password, grant_type="password")
        return token
    except KeycloakAuthenticationError:
        raise HTTPException(status_code=401, detail="Invalid credentials")


if __name__ == '__main__':
    _token = get_token_with_creds('NASA', 'nasa')
    print(_token)
