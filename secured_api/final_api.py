import http

from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from keycloak import KeycloakOpenID, KeycloakAuthenticationError
from starlette.responses import JSONResponse

################
# ## WORKING
################

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="nasa-client",
    realm_name="nasa-realm",
    client_secret_key="nasa_secret"
)


@app.get("/protected")
async def protected_route(token: str = Depends(oauth2_scheme)):
    protected_data = "PYTHON"
    try:
        token_info = keycloak_openid.userinfo(token)
        print(token_info)
    except KeycloakAuthenticationError:
        return JSONResponse(content={"message": "This is a protected route"}, status_code=403)
    return JSONResponse(content={"message": f"protected data : {protected_data}"}, status_code=200)


################################################################################################
# ## USE KC.DECODE_TOKEN
################################################################################################
# from pydantic import BaseModel
# from keycloak.exceptions import KeycloakGetError
#
# class ItemCreate(BaseModel):
#     name: str
# def get_current_user(token: str = Depends(keycloak_openid.decode_token)):
#     try:
#         user_info = keycloak_openid.userinfo(token=token)
#         return user_info
#     except KeycloakGetError:
#         raise HTTPException(status_code=401, detail="Unauthorized")
#
#
# @app.post("/secure_item/")
# def create_secure_item(item: ItemCreate, user: dict = Depends(get_current_user)):
#     # Your code to create a secure item goes here
#     return {"message": f"Item created by user: {user['preferred_username']}"}
#
#
# @app.get("/secure_items/")
# def get_secure_items(user: dict = Depends(get_current_user)):
#     # Your code to get secure items goes here
#     return {"message": f"Items retrieved by user: {user['preferred_username']}"}


################################################################################################
# ## USE MARSHMALLOW SQLALCHEMY
################################################################################################

# from marshmallow import Schema, fields
# from pydantic import BaseModel
# from sqlalchemy import create_engine, Column, Integer, String
# from sqlalchemy.orm import sessionmaker, declarative_base
# from typing import List
#
# DATABASE_URL = "postgresql://username:password@localhost/dbname"
# engine = create_engine(DATABASE_URL)
#
# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
#
# Base = declarative_base()
#
#
# # SQLAlchemy Model
# class Item(Base):
#     __tablename__ = "items"
#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String, index=True)
#
#
# # CREATE TABLE
# Base.metadata.create_all(bind=engine)
#
#
# # Marshmallow Schema
# class ItemSchema(Schema):
#     id = fields.Int()
#     name = fields.Str()
#
#
# class ItemCreate(BaseModel):
#     name: str
#
#
# # Endpoints
# @app.post("/items/", response_model=ItemSchema)
# def create_item(item: ItemCreate):
#     db_item = Item(name=item.name)
#     with SessionLocal() as session:
#         session.add(db_item)
#         session.commit()
#         session.refresh(db_item)
#     return db_item
#
#
# @app.get("/items/", response_model=List[ItemSchema])
# def get_items():
#     with SessionLocal() as session:
#         items = session.query(Item).all()
#     return items


################################################################################################


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8017)
