import keycloak
import requests
from fastapi import HTTPException
from keycloak import KeycloakOpenID

########################################################################################################################
# Keycloak
########################################################################################################################
keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="nasa-client",
    realm_name="nasa-realm",
    client_secret_key="nasa_secret"
)

VALID_CREDS = ('NASA', 'nasa')
BAD_CREDS = ('nasa', '123')
ALTERED_TOKEN = lambda __token: f'nasa{__token}'

# print("http://localhost:8080/realms/nasa-realm/protocol/openid-connect/token")


########################################################################################################################
# Core methods
########################################################################################################################
def authenticate_user(username: str, password: str):
    try:
        token = keycloak_openid.token(username, password, grant_type="password")
        return token
    except keycloak.exceptions.KeycloakAuthenticationError:
        raise HTTPException(status_code=401, detail="Invalid credentials")


# ASYNC, pip install & import
# import httpx
# import asyncio

# async def access_protected_route_httpx(_token):
#     async with httpx.AsyncClient() as client:
#         headers = {
#             "Authorization": f"Bearer {_token}"
#         }
#         response = await client.get("http://localhost:8017/protected", headers=headers)

#     if response.status_code == 200:
#         return response.json()
#     else:
#         print(f'Error : {response.status_code} : {response.json()}')
#         raise HTTPException(status_code=response.status_code, detail="Failed to access the protected route")


def access_protected_route(token):
    headers = {
        "Authorization": f"Bearer {token}"
    }

    response = requests.get("http://localhost:8017/protected", headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        # print(f'Error   : {response.status_code} : {response.json()}')
        raise HTTPException(status_code=response.status_code, detail="Failed to access the protected route")


########################################################################################################################
# Test Method 1
########################################################################################################################
def valid_cred():
    """Valid credentials, unchanged Token"""
    token_dict = authenticate_user(*VALID_CREDS)
    token = token_dict.get('access_token')
    # result = asyncio.run(access_protected_route_httpx(token))
    result = access_protected_route(token)
    return result


def bad_cred():
    """Bad credentials, no Token from Keycloak"""
    token_dict = authenticate_user(*BAD_CREDS)
    token = token_dict.get('access_token')
    # result = asyncio.run(access_protected_route(token))
    result = access_protected_route(token)
    return result


def bad_token():
    """Valid credentials, altered Token"""
    token_dict = authenticate_user(*VALID_CREDS)
    token = ALTERED_TOKEN(token_dict.get('access_token'))
    # result = asyncio.run(access_protected_route(f'nasa{token}'))
    result = access_protected_route(token)
    return result


def loop_on_test():
    for _test in [valid_cred, bad_cred, bad_token]:
        print('------------------------------')
        print(f'State   : {_test.__name__}(), {_test.__doc__}.')
        try:
            result = _test()
            print(f'Result  : {result}')
        except HTTPException as e:
            print(f'Result  : {e.status_code, e.detail}')
        print('------------------------------')


########################################################################################################################
# Test Method 2
########################################################################################################################
def try_to_access_api(_user, _password, _altered_token=False):
    _token_dict = authenticate_user(_user, _password)
    _token = _token_dict.get('access_token')
    if _altered_token:
        _token = ALTERED_TOKEN(_token)
    result = access_protected_route(_token)
    return result

def loop_on_cases():
    _cases = [
        [*VALID_CREDS, False, 'Valid credentials, Unchanged Token'],
        [*BAD_CREDS, False, 'Bad credentials, no token provided by Keycloak'],
        [*VALID_CREDS, True, 'Valid credentials, Altered Token'],
    ]
    for _id, _case in enumerate(_cases):
        print('------------------------------')
        _user, _password, _altered_token, _descr = _case
        print(f'Case {_id}  : {_descr}')
        try:
            result = try_to_access_api(_user, _password, _altered_token)
            print(f'Result  : {result}')
        except HTTPException as e:
            print(f'Result  : {e.status_code, e.detail}')
    print('------------------------------')

########################################################################################################################
# Execution
########################################################################################################################
if __name__ == '__main__':
    loop_on_cases()
    loop_on_test()
