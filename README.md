# Data Management Project

Ce projet est réalisé dans le cadre du module de cours Data Management lors de la formation `Expert en Informatique et Systèmes
d'Informations 1ère année` (EPSI Rennes 2023-2024 - Module `EISI3.1`, `EISI3.6`& `EISI3.8`).

![data_management_process](_readme_asset/data_management_process.png)

La source de données choisie pour ce projet est l'API de la NASA, en commencant par les données suivrantes : `https://ssd-api.jpl.nasa.gov/fireball.api?limit=20`.

Différents sous-projets sont implémentés afin de répresenter le cycle de vie de la donnée, parmi lesquels :

- Déploiment de bases de données (Elastic, Postgres) avec Docker
- API d'accès aux données (Fastapi)
- Solution d'IAM (Keycloak)
- Différents scripts de traitement et visualisation (pandas, numpy, matplotlib) 
- Application TkInter.

## How to run :

### Linux

```sh
git clone https://gitlab.com/Bob-117/nasa-data-management.git && cd nasa-data-management
python3.10 -m venv venv
source venv/bin/activate
```

<details>
<summary>Configuration & Versions</summary>

```sh
# Ubuntu
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.5 LTS
Release:	20.04
Codename:	focal

# Docker
Docker version 20.10.23, build 7155243

# Compose
Docker Compose version v2.15.1

# Python (venv)
Python 3.10.13

# Gnome (See Makefile)
GNOME Terminal 3.36.2 using VTE 0.60.3 +BIDI +GNUTLS +ICU +SYSTEMD
```

</details>


#### 1 - Elastic, Postgres & Delivery API with Docker 

````shell
docker compose -f _docker/docker-compose.yml up
````

<ins>- Postgres</ins>

```shell
psql -U myuser -h localhost -d mydb

mydb=# \dt
          List of relations
 Schema |   Name    | Type  | Owner  
--------+-----------+-------+--------
 public | fire_ball | table | myuser
(1 row)

mydb=# select * from fire_ball;
 id | date | energy | impact_e | lat | lat_dir | lon | lon_dir | alt | vel 
----+------+--------+----------+-----+---------+-----+---------+-----+-----
(0 rows)

```

<ins> - Delivery API</ins>

```sh
make delivery_api_up
```

```sh
curl 0.0.0.0:8001/ping
```

```sh
# If Postgres is up (run compose to have Elastic + Postgres + Delivery API)
curl 0.0.0.0:8001/fireballs
```

<ins> - Elastic</ins>

```sh
Memory restrictions have been defined for the Elastic container.
Given the memory constraints on the dev computer (>7.67GB), Elastic + Postgres + Keycloak + anything more can lead to an excessive memory consumption.
Update the docker-compose.yml for ur machine.
```

#### 2 - Keycloak

<ins> - Keycloak & NASA Realm</ins>

```sh
./keycloak/keycloak.sh
# Need to provide a realm.json file, beside the script location
# It will run Keycloak Docker container, wait its ready, and create a base Nasa Realm (one realm, one client, one user)
# A timeout is defined in the script to avoid some random issues:)
# (~ 100s from scratch, can timeout 180s with the deploy_gnome - c.f below or Makefile)
```

<details>

<summary>What it looks like</summary>

```shell
************************************
** DOCKER RUN NASA KEYCLOAK
************************************
eb1293512a3e2021f82f0a8b0f1f06c2bd4ed6cc4513e6c43242d9fd3376244f
************************************
** WAITING FOR KEYCLOAK INSTANCE
************************************
eb1293512a3e
2023-10-23 21:20:35 : Container is < running > but not ready for realm creation.
Last log : 
Checking again in 5 seconds (180 s max)...
------------------------------------------------------------------------
eb1293512a3e
2023-10-23 21:20:40 : Container is < running > but not ready for realm creation.
Last log : Updating the configuration and installing your custom providers, if any. Please wait.
Checking again in 5 seconds (180 s max)...
------------------------------------------------------------------------
######################
------------------------------------------------------------------------
eb1293512a3e
2023-10-23 21:21:47 : Container is < running > but not ready for realm creation.
Last log : 2023-10-23 19:21:45,297 INFO  [org.keycloak.services] (main) KC-SERVICES0050: Initializing master realm
Checking again in 5 seconds (180 s max)...
------------------------------------------------------------------------
eb1293512a3e
************************************
** Configure Keycloak realm
************************************
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1887  100  1818  100    69   2011     76 --:--:-- --:--:-- --:--:--  2085
------------------------------------------------------------------------
Create the realm in Keycloak
------------------------------------------------------------------------
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   460    0     0  100   460      0    224  0:00:02  0:00:02 --:--:--   224
------------------------------------------------------------------------
The realm is created. 
Open following link in your browser:
http://localhost:8080/admin/master/console/#/nasa-realm
------------------------------------------------------------------------

# docker logs -f nasa_keycloak
```

</details>

<br>

<ins> - Use Keycloak with python API & Client (light)</ins>


```shell
(venv) python secured_api/final_api.py
INFO:     Started server process [284272]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://0.0.0.0:8017 (Press CTRL+C to quit)
INFO:     127.0.0.1:40416 - "GET /protected HTTP/1.1" 200 OK
INFO:     127.0.0.1:40418 - "GET /protected HTTP/1.1" 403 Forbidden
```

```shell
(venv) python secured_api/final_client.py
------------------------------
Case 0  : Valid credentials, Unchanged Token
Result  : {'message': 'protected data : PYTHON'}
------------------------------
Case 1  : Bad credentials, no token provided by Keycloak
Result  : (401, 'Invalid credentials')
------------------------------
Case 2  : Valid credentials, Altered Token
Result  : (403, 'Failed to access the protected route')
------------------------------
```


#### TkInter App

```sh
(venv) python nasa_app/main.py
# dummy auth is hardcoded admin/1234 for debug so u can log without keycloak
```


<details>

<summary>What it looks like</summary>


![](_readme_asset/nasa_app_log_1.png)

</details>
<br>


Todo :
- [x] First frame `START`
- [x] Log frame => log to Keycloak 
- [ ] Source : register NASA API
- [ ] Raw Storage : register Elastic
- [ ] Clean Storage : register Postgres
- [ ] Connect API with token
- [ ] Pandas
- [ ] Numpy
- [ ] Matplotlib (plot + save png ?)

#### Kill

```sh
./_docker/kill_nasa_container.sh
```

#### Gnome Deploy

```sh
make deploy_gnome
# Open a gnome terminal
# 1 Tab compose elastic pg api
# 1 Tab keycloak.sh
# 1 Tab Containers Stats
```


### Windows

- Windows 11
- Docker Desktop 4.17.1 
- PowerShell 5.1


```sh
git clone https://gitlab.com/Bob-117/nasa-data-management.git 
cd nasa-data-management
git checkout master
python -m venv venv
```


### Conseil d'utilisation :
- Lancez le docker-compose
- Puis le script Python pour récupérer les datas
- Tester http://localhost:8001/fireballs

- Lancer les différentes parties (containers) de application :

````shell
# PowerShell
docker compose -f _docker/docker-compose.yml up
````

- Tester que l'app run :
```sh
# PowerShell
curl http://localhost:8001/ping
```

- Si la BDD n'a jamais été initialisée, ou si vous souhaitez mettre à jour les données :

```sh
./venv/Scripts/pip install -r requirements.txt
./venv/Scripts/python base/main.py
```

- Lancer uniquement l'API (attention a avoir le port 8001 libre !) :

````sh
# PowerShell
make delivery_api_up
````

## Data Management

### Data

https://api.nasa.gov/

```shell
# Fire Balls
Registration n0 : {'date': '2023-10-16 17:14:52', 'energy': 3.0, 'impact-e': 0.1, 'lat': 6.5, 'lat-dir': 'S', 'lon': 65.4, 'lon-dir': 'W', 'alt': 35.3, 'vel': 19.8}
Registration n1 : {'date': '2023-10-14 22:55:26', 'energy': 5.4, 'impact-e': 0.18, 'lat': 8.4, 'lat-dir': 'S', 'lon': 161.9, 'lon-dir': 'W', 'alt': 37.0, 'vel': 19.3}
```

### Display :


<details>
<summary> First plot </summary>

![first plot](_readme_asset/first_plot.png)

</details>

<details>
<summary>TkInter App 0.0.1</summary>

![tk inter app](_readme_asset/tk_app.png)

</details>

<details>
<summary>TkInter App 0.0.2</summary>

![tk inter app](_readme_asset/nasa_app_log_1.png)

![tk inter app](_readme_asset/nasa_app_content_1.png)

</details>


## Dropbox & Cmd

https://vaultvision.com/blog/quick-guide-to-the-top-user-authentication-protocols

https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt

https://stackoverflow.com/questions/53635512/docker-compose-how-to-do-version-2-mem-limit-in-version-3

https://github.com/inception-project/inception/issues/2680

https://github.com/compose-spec/compose-spec/blob/master/spec.md#mem_limit

<details>
<summary>Elastic</summary>

08-Jun-2023 : https://geshan.com.np/blog/2023/06/elasticsearch-docker/

```sh
docker pull docker.elastic.co/elasticsearch/elasticsearch:8.8.0
docker network create elastic
docker run --name es-node01 --net elastic -p 9200:9200 -p 9300:9300 -t docker.elastic.co/elasticsearch/elasticsearch:8.8.0
curl --cacert http_ca.crt -u elastic:$ELASTIC_PASSWORD https://localhost:9200
```

```sh
docker run --rm --name elasticsearch_container -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "xpack.security.enabled=false" elasticsearch:8.8.0
```
</details>


<details>
<summary>Postgres</summary>

```sh
pip install sqlalchemy
# psycopg2 error
pip install psycopg2
# error Python.h
pip install wheel
pip install --upgrade setuptools
sudo apt-get install python3.10-dev
# gg
# sinon sudo apt-get install libpq-dev ca peut aider
```


```sh
# psql ou equivalent
➜  ~ psql -U myuser -h localhost -d mydb 
Password for user myuser: ****

# ---------------------

mydb=# \dt
Did not find any relations.

# ---------------------

mydb=# CREATE TABLE IF NOT EXISTS fire_ball (
           id SERIAL PRIMARY KEY,
           date TIMESTAMP,
           energy FLOAT,
           impact_e FLOAT,
           lat FLOAT,
           lat_dir CHAR(1),
           lon FLOAT,
           lon_dir CHAR(1),
           alt FLOAT,
           vel FLOAT
       );
CREATE TABLE

# ---------------------

mydb=# \dt
          List of relations
 Schema |   Name    | Type  | Owner  
--------+-----------+-------+--------
 public | fire_ball | table | myuser
(1 row)

# ---------------------

mydb=# select * from fire_ball;
 id | date | energy | impact_e | lat | lat_dir | lon | lon_dir | alt | vel 
----+------+--------+----------+-----+---------+-----+---------+-----+-----
(0 rows)

# ---------------------

mydb=# select * from fire_ball
mydb-# ;
 id |        date         | energy | impact_e | lat  | lat_dir |  lon  | lon_dir | alt  | vel  
----+---------------------+--------+----------+------+---------+-------+---------+------+------
  1 | 2023-10-16 17:14:52 |      3 |      0.1 |  6.5 | S       |  65.4 | W       | 35.3 | 19.8
  2 | 2023-10-14 22:55:26 |    5.4 |     0.18 |  8.4 | S       | 161.9 | W       |   37 | 19.3
  3 | 2023-09-28 20:32:35 |      5 |     0.16 | 13.2 | N       |  72.4 | E       |   37 |     
  4 | 2023-09-03 04:28:01 |    2.3 |    0.082 | 43.2 | S       | 102.2 | E       |   37 |     
(4 rows)

# ---------------------
```

```sql

CREATE TABLE IF NOT EXISTS fire_ball (
    id SERIAL PRIMARY KEY,
    date TIMESTAMP,
    energy FLOAT,
    impact_e FLOAT,
    lat FLOAT,
    lat_dir CHAR(1),
    lon FLOAT,
    lon_dir CHAR(1),
    alt FLOAT,
    vel FLOAT
);
```
</details>

<details>
<summary>Old logs</summary>


```sh
docker run --rm --memory=4g --name elasticsearch_container -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "xpack.security.enabled=false" elasticsearch:8.8.0
```
```sh
# python src/main.py
=====GATHER======
Gathered from NASA APi : {'signature': {'version': '1.0', 'source': 'NASA/JPL Fireball Data API'}, 'count': '5', 'fields': ['date', 'energy', 'impact-e', 'lat', 'lat-dir', 'lon', 'lon-dir', 'alt', 'vel'], 'data': [['2023-10-16 17:14:52', '3.0', '0.1', '6.5', 'S', '65.4', 'W', '35.3', '19.8'], ['2023-10-14 22:55:26', '5.4', '0.18', '8.4', 'S', '161.9', 'W', '37.0', '19.3'], ['2023-09-28 20:32:35', '5.0', '0.16', '13.2', 'N', '72.4', 'E', '37.0', None], ['2023-09-07 15:34:46', '2.1', '0.076', '27.0', 'N', '150.8', 'E', None, None], ['2023-09-03 04:28:01', '2.3', '0.082', '43.2', 'S', '102.2', 'E', '37.0', None]]}
=====SAVE======
Creating index fire_ball_raw in Elastic
Registration n0 : {'date': '2023-10-16 17:14:52', 'energy': 3.0, 'impact-e': 0.1, 'lat': 6.5, 'lat-dir': 'S', 'lon': 65.4, 'lon-dir': 'W', 'alt': 35.3, 'vel': 19.8}
Registration n1 : {'date': '2023-10-14 22:55:26', 'energy': 5.4, 'impact-e': 0.18, 'lat': 8.4, 'lat-dir': 'S', 'lon': 161.9, 'lon-dir': 'W', 'alt': 37.0, 'vel': 19.3}
Registration n2 : {'date': '2023-09-28 20:32:35', 'energy': 5.0, 'impact-e': 0.16, 'lat': 13.2, 'lat-dir': 'N', 'lon': 72.4, 'lon-dir': 'E', 'alt': 37.0, 'vel': None}
Registration n3 : {'date': '2023-09-07 15:34:46', 'energy': 2.1, 'impact-e': 0.076, 'lat': 27.0, 'lat-dir': 'N', 'lon': 150.8, 'lon-dir': 'E', 'alt': None, 'vel': None}
Registration n4 : {'date': '2023-09-03 04:28:01', 'energy': 2.3, 'impact-e': 0.082, 'lat': 43.2, 'lat-dir': 'S', 'lon': 102.2, 'lon-dir': 'E', 'alt': 37.0, 'vel': None}
=====TRANSFORM======
From Elastic :
{'date': '2023-10-16 17:14:52', 'energy': 3.0, 'impact-e': 0.1, 'lat': 6.5, 'lat-dir': 'S', 'lon': 65.4, 'lon-dir': 'W', 'alt': 35.3, 'vel': 19.8}
{'date': '2023-10-14 22:55:26', 'energy': 5.4, 'impact-e': 0.18, 'lat': 8.4, 'lat-dir': 'S', 'lon': 161.9, 'lon-dir': 'W', 'alt': 37.0, 'vel': 19.3}
{'date': '2023-09-28 20:32:35', 'energy': 5.0, 'impact-e': 0.16, 'lat': 13.2, 'lat-dir': 'N', 'lon': 72.4, 'lon-dir': 'E', 'alt': 37.0, 'vel': None}
{'date': '2023-09-07 15:34:46', 'energy': 2.1, 'impact-e': 0.076, 'lat': 27.0, 'lat-dir': 'N', 'lon': 150.8, 'lon-dir': 'E', 'alt': None, 'vel': None}
{'date': '2023-09-03 04:28:01', 'energy': 2.3, 'impact-e': 0.082, 'lat': 43.2, 'lat-dir': 'S', 'lon': 102.2, 'lon-dir': 'E', 'alt': 37.0, 'vel': None}
# pandas remove None Outliers
# save into postgres
=====SORT======
# numpy:^)
=====ANALYSIS======
# numpy matplolib
=====DELIVERY======
# API (fast?) CRUD marshmallow schema + authent
=====DISPLAY======
# matplolib
+----+---------------------+----------+------------+-------+-----------+-------+-----------+-------+-------+
|    | date                |   energy |   impact-e |   lat | lat-dir   |   lon | lon-dir   |   alt |   vel |
|----+---------------------+----------+------------+-------+-----------+-------+-----------+-------+-------|
|  0 | 2023-10-16 17:14:52 |      3   |      0.1   |   6.5 | S         |  65.4 | W         |  35.3 |  19.8 |
|  1 | 2023-10-14 22:55:26 |      5.4 |      0.18  |   8.4 | S         | 161.9 | W         |  37   |  19.3 |
|  2 | 2023-09-28 20:32:35 |      5   |      0.16  |  13.2 | N         |  72.4 | E         |  37   |       |
|  3 | 2023-09-07 15:34:46 |      2.1 |      0.076 |  27   | N         | 150.8 | E         |       |       |
|  4 | 2023-09-03 04:28:01 |      2.3 |      0.082 |  43.2 | S         | 102.2 | E         |  37   |       |
+----+---------------------+----------+------------+-------+-----------+-------+-----------+-------+-------+
=====EXPLOITATION======
# scikit learning
```
</details>
