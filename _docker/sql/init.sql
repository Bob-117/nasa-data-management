CREATE USER apiuser;
GRANT ALL PRIVILEGES ON DATABASE mydb TO apiuser;

CREATE TABLE IF NOT EXISTS fire_ball (
    id SERIAL PRIMARY KEY,
    date TIMESTAMP,
    energy FLOAT,
    impact_e FLOAT,
    lat FLOAT,
    lat_dir CHAR(1),
    lon FLOAT,
    lon_dir CHAR(1),
    alt FLOAT,
    vel FLOAT
);

-- TODO view, users, trigger..