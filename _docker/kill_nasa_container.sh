#!/bin/bash

container_names=("nasa-delivery-api" "nasa-postgres" "nasa-elastic" "nasa-keycloak")

stop_containers() {
    local container_name="$1"
    if docker ps -q --filter "name=${container_name}" 2>/dev/null; then
        echo "Stopping container: $container_name"
        docker stop "$container_name"
    else
        echo "Container $container_name is not running"
    fi
}

remove_containers() {
    local container_name="$1"
    if docker ps -q -a --filter "name=${container_name}" 2>/dev/null; then
        echo "Removing container: $container_name"
        docker rm "$container_name"
    else
        echo "Container $container_name is not found"
    fi
}


main() {
    for container_name in "${container_names[@]}"; do
        stop_containers "$container_name"
    done

    for container_name in "${container_names[@]}"; do
        remove_containers "$container_name"
    done

}

main
