from base.core import CustomLogger
from base.core.database import ElasticDataBase, PostgresDatabase
import matplotlib.pyplot as plt
import numpy as np

_log = CustomLogger('process')


def process():
    _log.info('start debug')
    try:
        el = ElasticDataBase()
    except Exception as e:
        _log.error(e)
        exit(117)
    else:
        _log.info(f'{list(el.all_index())}')
        el.create_index('huho', {})
        _log.info(f'{list(el.all_index())}')
        el.delete_index('huho')
        _log.info(f'{list(el.all_index())}')


def haha():
    from elasticsearch import Elasticsearch

    base = Elasticsearch("http://localhost:9201")

    if not base.ping():
        _log.info(5)


def delete_fire_ball():
    el = ElasticDataBase()
    el.delete_index('fire_ball_raw')


def how_many_data_in_elastic():
    el = ElasticDataBase()
    # for index in el.all_index():
    #     print(index)
    data = [hit['_source'] for hit in el.get_all_from_index_batch(index_name='fire_ball_raw')]
    print(len(data))
    print(data)
    print(len(data))

    # data = [hit['_source'] for hit in el.get_all_from_index_batch(index_name='huho')['hits']['hits']]
    # print(len(data))


def populate_elastic():
    el = ElasticDataBase()
    mappings = {
        "properties": {
            "name": {"type": "text"}
        }
    }

    el.create_index('huho', mappings)

    for elem in range(10):
        doc = {
            "name": f'name - {elem + 50}'
        }
        el.base.index(index='huho', body=doc, id=f'doc{elem + 50}')
        # print(a)

    for elem in range(10):
        doc = {
            "name": f'name - {elem + 100}'
        }
        el.base.index(index='huho', body=doc, id=f'doc{elem + 500}')

    how_many_data_in_elastic()


def insert_one_item(index_name, item_data, _id):
    """
    Insert a single item into the specified Elasticsearch index.

    :param index_name: The name of the Elasticsearch index.
    :param item_data: The data for the item to be inserted.
    """
    el = ElasticDataBase()
    try:
        el.base.index(index=index_name, body=item_data, id=_id)
        el.log.info(f'Successfully inserted item into {index_name}: {item_data}')
    except Exception as e:
        el.log.error(f'Failed to insert item into {index_name}: {e}')


def huho():
    el = ElasticDataBase()
    for index in el.all_index():
        print(index)
    data = [hit['_source'] for hit in el.get_all_from_index(index_name='fire_ball_raw')['hits']['hits']]
    print(data)
    # print(f'len : {len(data)}')


# for k in range(11):
#     insert_one_item('huho', {
#         "name": "New Item"
#     }, _id=k+30)
#     how_many_data_in_elastic()

# how_many_data_in_elastic()

def plot_one():
    pg_db = PostgresDatabase()
    r = pg_db.select_all_sql()
    print([_ for _ in r])


# plot_one()

def azeraretz():
    def plot_vector_surface(ax, start, end, color='b'):
        radius = 1
        surface_normal = start / np.linalg.norm(start)
        start = surface_normal * radius
        end = start + end

        ax.quiver(start[0], start[1], start[2], end[0], end[1], end[2], color=color)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    start = np.array([1, 0, 0])
    end = np.array([1, 1, 1])
    plot_vector_surface(ax, start, end, color='r')

    radius = 1
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    x = radius * np.outer(np.cos(u), np.sin(v))
    y = radius * np.outer(np.sin(u), np.sin(v))
    z = radius * np.outer(np.ones(np.size(u)), np.cos(v))
    ax.plot_surface(x, y, z, color='b', alpha=0.1)

    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])

    plt.show()


import tkinter as tk
from tkinter import ttk


# import hashlib
#
# input_string = "a"
#
# sha256 = hashlib.sha256()
#
# sha256.update(input_string.encode('utf-8'))
#
# hashed_string = sha256.hexdigest()
#
# print("SHA-256 Hash:", hashed_string)


def tk_auth():
    def __auth():
        user_name_value = form_entry_user.get()
        print("Username :", user_name_value)

    width = 800
    height = 600

    root = tk.Tk()
    root.geometry(f"{width}x{height}")
    root.title("tk")

    auth_frame = ttk.Frame(root)
    auth_frame.grid(row=2, column=2, padx=10, pady=10)

    form_label_user = tk.Label(auth_frame, text="User :", width=10)
    form_entry_user = tk.Entry(auth_frame, width=10)
    form_label_user.grid(row=0, column=0, padx=(((width / 2) - 100), 10), pady=((height / 2) - 100, 5))
    form_entry_user.grid(row=0, column=1, pady=((height / 2) - 100, 5))

    form_label_pass = tk.Label(auth_frame, text="Pass :", width=10)
    form_entry_pass = tk.Entry(auth_frame, width=10)
    form_label_pass.grid(row=1, column=0, padx=(((width / 2) - 100), 10))
    form_entry_pass.grid(row=1, column=1)

    auth_button = tk.Button(auth_frame, text="Auth", command=__auth, width=10)
    auth_button.grid(row=2, sticky="sw", pady=50, padx=(((width / 2) + HERE), 0))

    root.mainloop()


# tk_auth()


class MainWidgetApp:
    def __init__(self, master):
        self.frame = tk.Frame(master)
        # self.populate_frame()
        self.label = tk.Label(self.frame, text='huho')
        self.label.grid()
        self.label.grid_remove()
        self.hide()

    def populate_frame(self):
        print('populate')
        self.label = tk.Label(self.frame, text='huho')
        self.label.grid()
        self.label.grid_remove()

    def show(self):
        print('show')
        self.frame.grid()
        self.label.grid()

    def hide(self):
        self.frame.grid_remove()


####################


class SubApp:
    def __init__(self, text_button, master):
        self.master = master
        self.text_button = text_button
        self.content = f'Content {text_button}'
        self.frame = None

    def create_frame(self):
        self.frame = tk.Frame(self.master)
        # label = tk.Label(self.frame, text=self.content)
        # label.grid()
        self.frame.grid(row=1, column=0)

    def show(self):
        self.frame.grid()

    def hide(self):
        self.frame.grid_remove()

    def __str__(self):
        return f'Sub app: {self.text_button}, content {self.content}'


def create_app_frame(master, __sub_apps):
    def switch_sub_app(__sub_app):
        for app in __sub_apps:
            app.hide()
        __sub_app.show()

    menu_frame = tk.Frame(master)
    menu_frame.grid(row=0, column=0)

    for idx, sub_app in enumerate(__sub_apps):
        button = tk.Button(menu_frame, text=sub_app.text_button, command=lambda app=sub_app: switch_sub_app(app))
        button.grid(row=0, column=idx, padx=(60, 0))
        sub_app.create_frame()
    menu_frame.grid_remove()
    switch_sub_app(__sub_apps[0])

    return master


####################"

def __set_app_frame(self):
    self.app_frame = ttk.Frame(self.root)

    nav_bar = ttk.Frame(self.app_frame)
    nav_bar.grid(row=0, column=0, sticky="w")

    self.sub_frames = []

    for label in ['A', 'B', 'C']:
        button = ttk.Button(nav_bar, text=f"Button {label}", command=lambda l=label: self.display_sub_frame(l))
        button.grid(row=0, column=len(self.sub_frames), padx=10)

        sub_frame = self.create_sub_frame(label)
        self.sub_frames.append(sub_frame)

    self.app_frame.grid(row=1, column=0)
    for sub_frame in self.sub_frames:
        sub_frame.grid(row=1, column=0, sticky="nsew")

    self.show_sub_frame(self.sub_frames[0])


def display_sub_frame(self, label):
    for i, sub_frame in enumerate(self.sub_frames):
        # if label == self.sub_frame_labels[i]:
        if label == ['A', 'B', 'C'][i]:
            self.show_sub_frame(sub_frame)


def create_sub_frame(self, label):
    sub_frame = ttk.Frame(self.app_frame)
    return sub_frame


def show_sub_frame(self, sub_frame):
    for frame in self.sub_frames:
        frame.grid_remove()
    sub_frame.grid()
