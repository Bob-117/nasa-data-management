import logging


class CustomLogger:
    def __init__(self, name, log_to_file=True, log_to_console=True):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        if log_to_file:
            fh = logging.FileHandler(f'{name}.log')
            fh.setLevel(logging.INFO)
            fh.setFormatter(formatter)
            self.logger.addHandler(fh)

        if not log_to_console:
            self.logger.propagate = False

    def info(self, message):
        self.logger.info(message)

    def error(self, message):
        self.logger.error(message)

    def warning(self, message):
        self.logger.warning(message)

    def debug(self, message):
        self.logger.debug(message)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log1 = CustomLogger('log1', log_to_file=True, log_to_console=False)
    log2 = CustomLogger('log2', log_to_file=False, log_to_console=True)
    log1.info('info from log 1')
    log2.info('info from log 2')
