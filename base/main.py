# TO REMOVE
from core import PostgresDatabase, gather_process, save_process, transform_process


def main_process():
    raw_data = gather_process()
    if raw_data:
        save_process(input_data=raw_data)
        transformed_data = transform_process()
        # TODO
        pg_db = PostgresDatabase()
        pg_db.easy_df(_dataframe=transformed_data)
        exit(117)
        # sorted_data = sort_process(data)
        # save_process(data)
        # analysis_process(data)
        # delivery_process(data)
        # display_process(data)
        # exploitation_process(data)


if __name__ == '__main__':
    main_process()
