import logging

# es_logger1 = logging.getLogger('elasticsearch')
# es_logger1.setLevel(logging.ERROR)
# es_logger2 = logging.getLogger('elastic_transport')
# es_logger2.setLevel(logging.ERROR)


class CustomLogger:
    def __init__(self, name):
        self.logger = logging.getLogger(name)
        logging.basicConfig(
            level=logging.INFO
        )

    def info(self, message):
        self.logger.info(message)
    
    def error(self, message):
        self.logger.error(message)

    def warning(self, message):
        self.logger.warning(message)
