# Standard
import requests
from tabulate import tabulate

from .logger import CustomLogger
# Custom
from .sort import raw_data_to_panda

# Const
API_KEY = ""
# MIN_VALUE = 10

# Astronomy Picture of the Day
url_astronomy_picture_of_the_day = "https://api.nasa.gov/planetary/apod?count=1"

# Fire Ball
fire_ball_limit_5 = 'https://ssd-api.jpl.nasa.gov/fireball.api?limit=20'
fire_ball_since_2014 = 'https://ssd-api.jpl.nasa.gov/fireball.api?date-min=2014-01-01'

# Near Earth Object Web Service
NeoWs = f"https://api.nasa.gov/neo/rest/v1/feed?start_date=2023-01-01&end_date=2023-01-05&api_key={API_KEY}"

gather_logger = CustomLogger('gather_logger')


def __get_data_from_api(_url, use_api_key=False):
    """
    Fetch one api url to gather data
    :param _url:
    :param use_api_key:
    :return:
    """
    gather_logger.info(f'Fetching API on {_url}')
    params = {
        "api_key": API_KEY
    }
    try:
        response = requests.get(_url, params=params) if use_api_key else requests.get(_url)
        if response.status_code == 200:
            data = response.json()
            return data
        else:
            gather_logger.error(f'Error : {response.status_code}')
    except requests.exceptions.ConnectionError as e:
        gather_logger.error('Avec internet c\'est mieux..')


def gather_process():
    # __data = __get_data_from_api(url_astronomy_picture_of_the_day, use_api_key=True)
    # __gathered_is_potentially_hazardous_asteroid = __get_data_from_api(NeoWs)
    __gathered_fireball_data = __get_data_from_api(fire_ball_limit_5)
    gather_logger.info(f'Gathered from NASA APi : {__gathered_fireball_data}')
    if __gathered_fireball_data:  # todo
        df = raw_data_to_panda(__gathered_fireball_data)
        gather_logger.info('\n' + tabulate(df, headers='keys', tablefmt='psql'))
    # print(f'Gathered 2 : {__gathered_is_potentially_hazardous_asteroid}')
    return __gathered_fireball_data
