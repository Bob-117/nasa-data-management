import math

import numpy as np
from matplotlib import pyplot as plt


def display_process(data):
    print(f'display')


def calculate_end_vector(start, lat, lat_dir, lon, lon_dir, alt):
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)

    a = 6378.1  # Semi-major axis of the Earth in km
    b = 6356.8  # Semi-minor axis of the Earth in km

    R = 1 / math.sqrt((math.cos(lat_rad) / a) ** 2 + (math.sin(lat_rad) / b) ** 2)

    alt_km = alt / 1000  # km

    # Cartesian coordinates
    x = (R + alt_km) * math.cos(lat_rad) * math.cos(lon_rad)
    y = (R + alt_km) * math.cos(lat_rad) * math.sin(lon_rad)
    z = (R + alt_km) * math.sin(lat_rad)

    # todo
    if lat_dir == 'S':
        x = -x
        y = -y
        z = -z
    if lon_dir == 'W':
        y = -y
        x = -x

    end = [start[0] + x, start[1] + y, start[2] + z]
    return end


def plot_vectors():
    def __plot_sphere(_ax, radius=1, color='r', alpha=0.8):
        u, v = np.mgrid[0:2 * np.pi:20j, 0:np.pi:10j]
        x = radius * np.cos(u) * np.sin(v)
        y = radius * np.sin(u) * np.sin(v)
        z = radius * np.cos(v)
        _ax.plot_wireframe(x, y, z, color='blue', alpha=alpha)

    def __plot_vector(_ax, _start, _end, color='b'):
        """
        3d vector
        :param _ax:
        :param _start:
        :param _end:
        :param color:
        :return:
        """
        _ax.quiver(_start[0], _start[1], _start[2], _end[0], _end[1], _end[2], color=color)

    fig = plt.figure(figsize=(4, 4))
    ax = fig.add_subplot(111, projection='3d')

    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])

    __plot_sphere(ax, radius=1, color='w')

    fireballs = [
        {'date': '2023-04-15 08:22:01', 'energy': 309.7, 'impact-e': 6.3, 'lat': 1, 'lat-dir': 'S', 'lon': 10,
         'lon-dir': 'E', 'alt': 41.4, 'vel': 17.2},
        {'date': '2023-04-06 14:47:39', 'energy': 7.2, 'impact-e': 0.23, 'lat': 150, 'lat-dir': 'S', 'lon': 150,
         'lon-dir': 'E', 'alt': 31.2, 'vel': 22.1},
        {'date': '2023-04-01 00:02:03', 'energy': 2.4, 'impact-e': 0.086, 'lat': 0, 'lat-dir': 'N', 'lon': 0,
         'lon-dir': 'W', 'alt': 61.8, 'vel': 46.4}
    ]

    # for fireball in fireballs:
    #     start = [0, 0, 0]
    #     end = [fireball['lat'], fireball['lon'], fireball['alt']]
    #     plot_vector(ax, start, end, color='r')

    for n, fireball in enumerate(fireballs):
        # todo color ~ velocity ?
        _color = 'red'
        if n == 2:
            _color = 'green'
        # todo calculate_start_vector earth surface
        end = calculate_end_vector([0, 0, 0], fireball['lat'], fireball['lat-dir'], fireball['lon'],
                                   fireball['lon-dir'],
                                   fireball['alt'])
        __plot_vector(ax, [0, 0, 0], end, color=_color)

    fig.suptitle('Fireballs Trajectories')
    # todo axes name
    # plt.show()
    return fig  


def plot_sphere():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    r = 6371  # Earth's radius km
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    x = r * np.outer(np.cos(u), np.sin(v))
    y = r * np.outer(np.sin(u), np.sin(v))
    z = r * np.outer(np.ones(np.size(u)), np.cos(v))
    ax.plot_surface(x, y, z, color='b', alpha=0.2)

    fireballs = [
        {'date': '2023-04-15 08:22:01', 'energy': 309.7, 'impact-e': 6.3, 'lat': 20.1, 'lat-dir': 'S', 'lon': 36.0,
         'lon-dir': 'E', 'alt': 41.4, 'vel': 17.2},
        {'date': '2023-04-06 14:47:39', 'energy': 7.2, 'impact-e': 0.23, 'lat': 57.4, 'lat-dir': 'N', 'lon': 109.9,
         'lon-dir': 'E', 'alt': 31.2, 'vel': 22.1},
        {'date': '2023-04-01 00:02:03', 'energy': 2.4, 'impact-e': 0.086, 'lat': 16.8, 'lat-dir': 'S', 'lon': 76.0,
         'lon-dir': 'E', 'alt': 61.8, 'vel': 46.4}
    ]

    ax.set_xlim([-r * 2, r * 2])
    ax.set_ylim([-r * 2, r * 2])
    ax.set_zlim([-r * 2, r * 2])
    ax.set_xlabel('Longitude')
    ax.set_ylabel('Latitude')
    ax.set_zlabel('Altitude')

    plt.show()


if __name__ == '__main__':
    plot_vectors()
