# Elastic
from elasticsearch import Elasticsearch, BadRequestError as el_BadRequestError
# Postgres
from sqlalchemy import create_engine, text

# Custom
from .logger import CustomLogger


class Database:
    def __init__(self, target):
        self.log = CustomLogger(target)

    def engine(self):
        ...


class DatabaseException(Exception):
    ...


class ElasticDataBase(Database):

    def __init__(self, _host='localhost', _port=9200) -> None:
        self.target = "Elastic"
        super().__init__(self.target)
        self.base = Elasticsearch(f'http://{_host}:{_port}')
        if not self.base.ping():
            raise DatabaseException(f'Could not connect to Elasticsearch on http://{_host}:{_port}')

    def create_index(self, index_name, mappings):
        """
        Create one index in ElasticSearch Base
        """
        try:
            self.log.info(f'Creating index {index_name}')
            self.base.indices.create(index=index_name, body={"mappings": mappings})
        except el_BadRequestError as e:
            self.log.error(f'Nope : {e.message}')

    def all_index(self):
        """
        Retrieve all index in the ElasticSearch Base instance
        :return:
        """
        res = self.base.cat.indices()
        # self.log.info(f'All index : {res}')
        for line in res.splitlines():
            tmp_index_name = line.split()[2]
            yield tmp_index_name

    def get_all_from_index(self, index_name='fire_ball_raw'):
        """
        Retrieve all document for a given index in ElasticSearch Base
        :param index_name:
        :return:
        """
        res = self.base.search(index=index_name, body={"query": {"match_all": {}}}, size=100)
        self.log.info(f'From Elastic : ')
        for hit in res['hits']['hits']:
            self.log.info(hit)
            # self.log.info(hit['_source'])
            # yield hit['_source']
        return res

    def get_all_from_index_batch(self, index_name='fire_ball_raw', batch_size=10):
        """
        Retrieve all documents for a given index in Elasticsearch Base in batches
        :param index_name:
        :param batch_size: Number of documents to retrieve in each batch (default: 10)
        :return:
        """
        total_hits = self.how_many_docs_in_index(index_name=index_name)
        self.log.info(f'Total documents: {total_hits}')

        batches = total_hits // batch_size
        remaining_docs = total_hits % batch_size

        for batch_num in range(batches):
            res = self.base.search(index=index_name, body={"query": {"match_all": {}}}, size=batch_size,
                                   from_=batch_num * batch_size)
            self.log.info(f'Batch {batch_num + 1}:')
            for hit in res['hits']['hits']:
                # self.log.info(hit)
                yield hit

        if remaining_docs > 0:
            res = self.base.search(index=index_name, body={"query": {"match_all": {}}}, size=remaining_docs,
                                   from_=batches * batch_size)
            self.log.info(f'Remaining documents:')
            for hit in res['hits']['hits']:
                # self.log.info(hit)
                yield hit

    def delete_index(self, index_name):
        """
        Delete one given index in ElasticSearch Base
        :param index_name:
        :return:
        """
        self.log.info(f'Deleting index {index_name}')
        self.base.indices.delete(index=index_name)

    def insert_data(self, index_name, input_data):
        """
        Insert series or data for a given index in ElasticSearch Base
        :param index_name:
        :param input_data:
        :return:
        """
        data = input_data
        for _id, row in enumerate(data['data']):
            doc = {
                "date": row[0],
                "energy": float(row[1]),
                "impact-e": float(row[2]),
                "lat": float(row[3]),
                "lat-dir": row[4],
                "lon": float(row[5]),
                "lon-dir": row[6],
                "alt": float(row[7]) if row[7] is not None else None,
                "vel": float(row[8]) if row[8] is not None else None
            }
            self.log.info(f'Registration n{_id} : {doc}')
            self.base.index(index=index_name, body=doc)

    def how_many_docs_in_index(self, index_name):
        tmp_res = self.base.search(index=index_name, body={"query": {"match_all": {}}}, size=1)
        total_hits = tmp_res['hits']['total']['value']
        return total_hits


class PostgresDatabase(Database):
    def __init__(self):
        self.target = "Postgres"
        super().__init__(self.target)
        self.conn_string = 'postgresql://myuser:mypassword@localhost:5432/mydb'
        self.engine = create_engine(self.conn_string)

    def select_all_sql(self):
        with self.engine.connect() as con:
            stmt = """SELECT * FROM fire_ball"""
            result = con.execute(text(stmt))
            rows = result.fetchall()

        self.log.info('From Postgres :')
        for row in rows:
            self.log.info(row)
            yield row

    def prepared_stmt(self):
        connection = self.engine.connect()
        sql = 'SELECT * FROM my_table WHERE id = :id'
        stmt = text(sql)
        res = connection.execute(stmt, {'id': 123})
        self.log.info(f'Executed SQL {sql} : {res}')
        connection.close()
        return res

    def easy_df(self, _dataframe, table='fire_ball'):
        connection = self.engine.connect()
        _dataframe.to_sql(table, self.engine, if_exists='append', index=False)
        connection.close()
