import pandas as pd
from tabulate import tabulate

from .database import ElasticDataBase
from .logger import CustomLogger

transform_logger = CustomLogger('transform_logger')


def transform_process():
    transform_logger.info(f'Transform')

    # Get back data from raw storage
    df = __get_back_from_raw_storage()

    # Clean the data
    df_without_none = __remove_none(df)
    transform_logger.info('df without none')
    transform_logger.info('\n' + tabulate(df_without_none[0:15], headers='keys', tablefmt='psql'))

    df_without_none = df_without_none.rename(columns=lambda x: x.replace('-', '_'))  # postgres issues
    return df_without_none


def __get_back_from_raw_storage():
    """
    TODO
    :return:
    """
    transform_logger.info(f'======= Getting data from raw storage ')
    out = None
    try:
        el = ElasticDataBase()
    except Exception as e:
        transform_logger.error(e)
    else:
        transform_logger.info(f'{list(el.all_index())}')
        data = [hit['_source'] for hit in el.get_all_from_index_batch(index_name='fire_ball_raw')]
        out = pd.DataFrame(data)
        transform_logger.info('\n' + tabulate(out[0:15], headers='keys', tablefmt='psql'))
    transform_logger.info(f'===> Found {len(out)} docs')
    return out

    # res = es.get(index="my_index", id="1")

    # print(res['_source'])


def __remove_none(df):
    """
    Remove row with NaN
    :param df:
    :return:
    """
    before = len(df)
    df_without_none = df.dropna(thresh=len(df.columns))
    transform_logger.info(f'===> remove none from {before} to {len(df_without_none)}')
    return df_without_none


def __outliers():
    ...
