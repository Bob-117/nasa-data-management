# TK
import tkinter as tk
from tkinter import ttk

# Actions
import subprocess
from .gather import gather_process
from .display import plot_vectors
# Plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

# to remove
from time import sleep

# Use the global variable
matplotlib_figure = None
canvas_widget = None
close_matplotlib_button = None


def deploy_db():
    # TODO docker compose up + while not healthy
    print("TODO DOCKER COMPOSE UP")
    sleep(1.5)
    return True


def widget_app():
    def show_matplotlib_graph():
        # Use the global variable
        global matplotlib_figure
        global canvas_widget
        global close_matplotlib_button

        # close button
        close_matplotlib_button = ttk.Button(root, text="Close Matplotlib", command=close_matplotlib_graph)
        close_matplotlib_button.grid(row=3, column=0, padx=10, pady=10)

        # log init
        log_text.config(state=tk.NORMAL)
        log_text.insert(tk.END, "Generating Matplotlib graph...\n")

        # Matplotlib figure
        fig = Figure(figsize=(4, 4), dpi=100)
        plot = fig.add_subplot(111)
        plot.plot([1, 2, 3, 4, 5], [1, 2, 3, 4, 5])
        plot.set_xlabel('X-axis')
        plot.set_ylabel('Y-axis')
        plot.set_title('Matplotlib Graph')

        fig = plot_vectors()

        # Matplotlib fig in Tkinter canvas
        canvas = FigureCanvasTkAgg(fig, master=root)
        canvas_widget = canvas.get_tk_widget()
        canvas_widget.grid(row=2, column=0, padx=10, pady=10)

        # Matplotlib fig as global
        matplotlib_figure = fig

        # log end
        log_text.insert(tk.END, "Matplotlib graph displayed.\n")
        log_text.config(state=tk.DISABLED)
        auto_scroll()

    def close_matplotlib_graph():
        # Use global
        global matplotlib_figure
        global canvas_widget
        global close_matplotlib_button
        if matplotlib_figure:
            matplotlib_figure.clf()  # Clear Matplotlib fig
            canvas_widget.destroy()  # Destroy canvas
            close_matplotlib_button.grid_remove()  # Hide button
            matplotlib_figure = None

    def run_process(command):
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        return out, err

    def execute_step(step):
        if step == "huho":
            out, err = "huho", 'nope'
        elif step == "Retrieve Data from NASA API":
            out, err = gather_process(), 'gather_err'  # py method
        elif step == "compose up":
            out, err = run_process("python -V")  # process => docker compose up + while not healthy
        elif step == "Matplotlib":
            out, err = show_matplotlib_graph(), None  # matplotlib
        else:
            return

        log_text.config(state=tk.NORMAL)
        log_text.insert(tk.END, f"Step: {step}\n")
        log_text.insert(tk.END, f"Output:\n{out}\n")
        log_text.insert(tk.END, f"Error:\n{err}\n")  # decode subprocess err
        log_text.config(state=tk.DISABLED)
        auto_scroll()

    def show_additional_buttons_after_deploy_db():
        deploy_state = deploy_db()
        if deploy_state == True:
            initial_button.grid_forget()
            for button in additional_buttons:
                button.grid()
        else:
            exit(117)

    def auto_scroll():
        log_text.see(tk.END)

    root = tk.Tk()
    root.title("NASA Data Management")

    left_frame = ttk.Frame(root)
    right_frame = ttk.Frame(root)

    left_frame.grid(row=0, column=0, padx=10, pady=10, sticky=(tk.W, tk.E))
    right_frame.grid(row=0, column=1, padx=10, pady=10, sticky=(tk.W, tk.E))

    initial_button = ttk.Button(left_frame, text="Deploy Databases", command=show_additional_buttons_after_deploy_db)
    initial_button.grid(row=0, column=0, padx=5, pady=5, sticky=(tk.W, tk.E))

    steps = [
        "compose up",
        "Retrieve Data from NASA API",
        "Matplotlib",
        "huho"
    ]

    additional_buttons = []
    buttons_col1 = []
    buttons_col2 = []

    for i, step in enumerate(steps):
        button = ttk.Button(left_frame if step != 'huho' else right_frame, text=step, command=lambda step=step: execute_step(step))
        additional_buttons.append(button)
        if step == 'huho':
            buttons_col2.append(button)
        else:
            buttons_col1.append(button)
        button.grid(row=i + 1, column=0, padx=5, pady=5, sticky=(tk.W, tk.E))
        button.grid_remove()  # Hide additional_buttons

    log_text = tk.Text(root, wrap=tk.WORD, width=60, height=20)
    log_text.grid(row=1, column=0, padx=10, pady=10)
    log_text.config(state=tk.DISABLED)

    root.mainloop()
