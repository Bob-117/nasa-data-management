
from .logger import CustomLogger

save_logger = CustomLogger('save_logger')


def save_process(input_data):
    """
    Save raw data in ElasticSearch
    :param input_data:
    :return:
    """
    save_logger.info(f'======= Saving {len(input_data)}')
    __save_elastic(input_data)


def __save_elastic(input_data):
    # TODO use ElasticDataBase()
    from elasticsearch import Elasticsearch, BadRequestError as el_BadRequestError
    es = Elasticsearch("http://localhost:9200")
    index_name = "fire_ball_raw"
    mappings = {
        "properties": {
            "date": {"type": "text"},  # todo
            "energy": {"type": "float"},
            "impact-e": {"type": "float"},
            "lat": {"type": "float"},
            "lat-dir": {"type": "keyword"},
            "lon": {"type": "float"},
            "lon-dir": {"type": "keyword"},
            "alt": {"type": "float"},
            "vel": {"type": "float"}
        }
    }

    # def create_index():
    try:
        save_logger.info(f'Creating index {index_name}')
        es.indices.create(index=index_name, body={"mappings": mappings})
    except el_BadRequestError as e:
        save_logger.error(f'Nope : {e.message}')

    data = input_data
    for _id, row in enumerate(data['data']):
        # es.indices.refresh(index=index_name)
        # how_many_data_in_elastic()
        try:
            doc = {
                "date": row[0],
                "energy": float(row[1]),
                "impact-e": float(row[2]),
                "lat": float(row[3]),
                "lat-dir": row[4],
                "lon": float(row[5]),
                "lon-dir": row[6],
                "alt": float(row[7]) if row[7] is not None else None,
                "vel": float(row[8]) if row[8] is not None else None
            }
            save_logger.info(f'Registration n{_id} : {doc}')
            es.index(index=index_name, body=doc)
        except:
            save_logger.error(f'Cant save in elastic : \n{_id} : {row}')
