db_up:
	docker compose -f _docker/docker-compose.yml up


build_delivery_api:
	docker build -f delivery_api/Dockerfile -t delivery_api:1.0.0 delivery_api


delivery_api_up: build_delivery_api
	docker run --name nasa-delivery-api -p 8001:8001 --rm delivery_api:1.0.0


deploy_gnome :
	gnome-terminal --window \
	--tab --title="STATS" -e "bash -c 'docker stats --format \"table {{.Name}}\t{{.Container}}\t{{.CPUPerc}}\t{{.MemUsage}}\" '" \
	--tab --title="COMPOSE" -e "bash -c 'echo COMPOSE UP;make db_up; echo Compose; bash'" \
	--tab --title="KEYCLOAK" -e "bash -c 'echo KEYCLOAK; ./keycloak/keycloak.sh; echo End Keycloak; bash'"\


deploy_gnome_nope:
	gnome-terminal --tab --title="COMPOSE" -- bash -c 'echo COMPOSE UP;make db_up' \
	--tab --title="KEYCLOAK" -- bash -c 'echo KEYCLOAK; ./keycloak/keycloak.sh'


docker_stop_nasa:
	@docker ps -q --filter "name=nasa-elastic" | xargs -r docker stop


docker_rm_nasa: docker_stop_nasa
	@docker ps -q -a --filter "name=nasa-elastic" | xargs -r docker rm

